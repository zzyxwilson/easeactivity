package algorithm.longestdownhillpath;

import java.util.Comparator;

import algorithm.SkiPath;

public enum LongestDownhillPathComparator implements Comparator<SkiPath> {

	Instance;
	public static LongestDownhillPathComparator getInstance() {
		return Instance;
	}

	@Override
	public int compare(SkiPath path1, SkiPath path2) {
		int lengthComparison = Integer.compare(path1.getLengthOfCalculatedPath(), path2.getLengthOfCalculatedPath());
		int dropComparison = Integer.compare(path1.getDropOfCalculatedPath(), path2.getDropOfCalculatedPath());

		return (lengthComparison == 0) ? dropComparison : lengthComparison;
	}

}
