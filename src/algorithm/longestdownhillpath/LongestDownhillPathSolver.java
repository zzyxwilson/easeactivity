package algorithm.longestdownhillpath;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import algorithm.SkiPath;
import algorithm.SkiPathSolver;
import datasource.SkiMapDataSource;

public class LongestDownhillPathSolver implements SkiPathSolver {

	@Override
	public SkiPath solve(SkiMapDataSource dataSource) {
		return solve(dataSource.map(), dataSource.rows(), dataSource.columns());
	}

	private SkiPath solve(int[][] map, int rows, int columns) {

		Map<Integer, SkiPath> positionToPath = new HashMap<>();

		SkiPath longestPath = new SkiPath();
		for (int i = 0; i < map.length; i++) {
			for (int j = 0; j < map[i].length; j++) {
				SkiPath currentPath = getLongestPath(i, j, map, rows, columns, positionToPath);
				if (LongestDownhillPathComparator.getInstance().compare(currentPath, longestPath) > 0) {
					longestPath = currentPath;
				}
			}
		}
		return longestPath;
	}

	private SkiPath getLongestPath(int row, int column, int[][] map, int rows, int columns, Map<Integer, SkiPath> positionToPath) {
		int position = getPosition(row, column, columns);
		if (positionToPath.containsKey(position)) {
			return positionToPath.get(position);
		}

		List<Integer> path = new ArrayList<Integer>();
		path.add(map[row][column]);

		SkiPath longestNeighborPath = new SkiPath();
		int[] neighborRows = new int[] { row - 1, row, row + 1, row };
		int[] neighborColumns = new int[] { column, column - 1, column, column + 1 };

		for (int i = 0; i < neighborColumns.length; i++) {
			int neighborRow = neighborRows[i];
			int neighborColumn = neighborColumns[i];
			if (((neighborRow >= 0 && neighborRow < rows) && (neighborColumn >= 0 && neighborColumn < columns)) && (map[row][column] > map[neighborRow][neighborColumn])) {
				SkiPath neighborPath = getLongestPath(neighborRow, neighborColumn, map, rows, columns, positionToPath);
				if (LongestDownhillPathComparator.getInstance().compare(neighborPath, longestNeighborPath) > 0) {
					longestNeighborPath = neighborPath;
				}
			}
		}

		path.addAll(longestNeighborPath.getCalculatedPath());
		SkiPath longestPath = new SkiPath(path);
		positionToPath.put(position, longestPath);
		return longestPath;
	}

	private int getPosition(int row, int column, int columns) {
		return (columns * row) + column;
	}

}
