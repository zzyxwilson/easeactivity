package algorithm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SkiPath {

	private final List<Integer> path;

	public SkiPath() {
		this(null);
	}

	public SkiPath(List<Integer> path) {
		this.path = (path != null) ? Collections.unmodifiableList(path) : new ArrayList<Integer>();
	}

	public int getLengthOfCalculatedPath() {
		return this.path.size();
	}

	public int getDropOfCalculatedPath() {
		return (this.path.size() > 0) ? this.path.get(0) - this.path.get(this.path.size() - 1) : 0;

	}

	public List<Integer> getCalculatedPath() {
		return this.path;
	}

	@Override
	public String toString() {
		String str = "";
		for (int elevation : this.path) {
			str += "-" + elevation;
		}
		return (this.path.size() > 0) ? str.substring(1) : "N/A";
	}

}
