package algorithm;

import datasource.SkiMapDataSource;

public interface SkiPathSolver {
	
	SkiPath solve(SkiMapDataSource dataSource);

}
