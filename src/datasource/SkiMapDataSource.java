package datasource;

public interface SkiMapDataSource {

	public int rows();
	public int columns();
	public int[][] map();
	
}
