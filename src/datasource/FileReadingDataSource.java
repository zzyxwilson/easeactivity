package datasource;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class FileReadingDataSource implements SkiMapDataSource {

	private int rows;
	private int columns;
	private int[][] map;

	public FileReadingDataSource(String filename) throws FileNotFoundException {
		File inputFile = new File(filename);
		Scanner inputFileScanner = new Scanner(inputFile);

		this.rows = inputFileScanner.nextInt();
		this.columns = inputFileScanner.nextInt();
		this.map = new int[this.rows][this.columns];

		for (int i = 0; i < map.length; i++) {
			for (int j = 0; j < map[i].length; j++) {
				map[i][j] = inputFileScanner.nextInt();
			}
		}
		inputFileScanner.close();
	}

	@Override
	public int rows() {
		return rows;
	}

	@Override
	public int columns() {
		return columns;
	}

	@Override
	public int[][] map() {
		return map;
	}

}
