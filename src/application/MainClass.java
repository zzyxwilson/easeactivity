package application;

import java.io.FileNotFoundException;

import algorithm.SkiPathSolver;
import algorithm.longestdownhillpath.LongestDownhillPathSolver;
import datasource.FileReadingDataSource;
import datasource.SkiMapDataSource;

public class MainClass {

	private static final String FILENAME = "map.txt";

	public static void main(String[] args) {
		try {
			SkiMapDataSource dataSource = new FileReadingDataSource(FILENAME);
			SkiPathSolver pathSolver = new LongestDownhillPathSolver();
			SkiPathFinder pathFinder = new SkiPathFinder();

			pathFinder.solvePath(dataSource, pathSolver);

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

}
