package application;

import algorithm.SkiPath;
import algorithm.SkiPathSolver;
import datasource.SkiMapDataSource;

public class SkiPathFinder {

	public void solvePath(SkiMapDataSource dataSource, SkiPathSolver solver) {
		SkiPath path = solver.solve(dataSource);
		System.out.println("Length of calculated path = " + path.getLengthOfCalculatedPath());
		System.out.println("Drop of calculated path = " + path.getDropOfCalculatedPath());
		System.out.println("Calculated path = " + path.toString());
	}

}
